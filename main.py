# -*- coding: utf-8 -*-
"""
Created on Sun Dec 13 10:07:02 2020

@author: hasee
this is an openpyxl practice example
表格为：
序号	姓名	id	        成绩
1	A	20200001	97
2	B	20200002	50
3	C	20200003	60
4	D	20200004	54
"""
import openpyxl
#openpyxl库和xlrd\xlwt一样，用于完成py对excel表格的操作
#openpyxl一般用于新版本excel可向下兼容
wb =openpyxl.load_workbook(r'C:\Users\hasee\Desktop\python-excel.xlsx')
#读取指定位置的excel文件，r'C:\Users\hasee\Desktop\python-excel.xlsx'
print(wb.sheetnames)#.sheetnames获取工作簿的名字
for sheet in wb:
    print(sheet.title)
mysheet = wb.create_sheet('mysheet')#新增加一张工作簿'mysheet'
print(wb.sheetnames)
#sheet3 = wb.get_sheet_by_name('sheet3')#通过名称来调用工作簿
#sheet4 = wb.['mysheet']
ws = wb.active
print(ws)#打印显示正处于活跃状态的工作簿
print(ws['A1'].value)#打印活跃工作簿中的单元格'A1'的内容（value）“序号”
c = ws['B2']
print('(row{0},column{1})is {2}'.format(c.row,c.column,c.value))
#行列号引用，序号从0开始；使用.format方法进行打印，输出行号、列号和内容；“A”
print('Cell {} is {}\n'.format(c.coordinate,c.value))
#使用坐标进行引用
print(ws.cell(row=1,column=2).value)
#使用.cell方法，行列序号从1开始，cell相当于去除行标题和列标题，实际为B2内容为“A”
print("\n")
#取出整列整行
colC=ws['C']
for i in range(len(colC)):#len()返回对象（字符、列表、元组）的长度或项目的个数
    print(colC[i].value)
print("\n")

col_range=ws[1:3]
row_range=ws[2:5]
for col in col_range:
    for cell in col:
        print(cell.value)
#通过循环来访问单元格cell
print("\n")
for row in ws.iter_rows(min_row=2,max_row=5,max_col=3):
    for cell in row:
        print(cell)
#通过元组形式，循环来访问单元格cell
if __name__ == '__main__':
    print_hi('PyCharm')

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
